const { defaults } = require('jest-config');

module.exports = {
  cacheDirectory: '.jest-cache',
  coverageDirectory: 'coverage',
  coverageReporters: [...defaults.coverageReporters, 'html'],
  moduleNameMapper: {
    '\\.(jpg|jpeg|png|gif|eot|otf|webp|svg|ttf|woff|woff2|mp4|webm|wav|mp3|m4a|aac|oga)$':
      '<rootDir>/__mocks__/fileMock.js',
    '\\.(css|less)$': '<rootDir>/__mocks__/styleMock.js',
    '@organization/(.*)$': '<rootDir>/packages/$1'
  },
  setupFilesAfterEnv: ['<rootDir>/jest.setup.js'],
  transform: {
    '^.+\\.[t|j]sx?$': ['babel-jest', { rootMode: 'upward' }],
    '^.+\\.mdx$': '@storybook/addon-docs/jest-transform-mdx'
  },
  transformIgnorePatterns: ['node_modules/(?!(@organization)/)']
};
