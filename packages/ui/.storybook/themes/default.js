import { create } from '@storybook/theming';

export default create({
  base: 'light',

  brandTitle: 'Organization UI',
  brandUrl: 'https://gitlab.com/stickey/typescript-monorepo-template'
});
